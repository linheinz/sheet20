context("PCA-testing")

pca_100 = c(-0.3162278, -0.3162278, -0.3162278, -0.3162278, -0.3162278, -0.3162278, -0.3162278, -0.3162278, -0.3162278, -0.3162278)
pca_3 = c(-0.5773503, -0.5773503, -0.5773503)
test_that("PCA works", {
  expect_equal(principale_components(matrix(1:100, nrow=10),0.8), pca_100 , tolerance=4e-08)
  expect_equal(principale_components(matrix(1:9, nrow=3),0.8), pca_3, tolerance=4e-08)
})

context("Number recognition")
test_that("which_number works", {
  expect_equal(which_number(test[22,],0.8), as.numeric(as.character(test$y[[22]])))
  expect_equal(which_number(test[23,],0.7), as.numeric(as.character(test$y[[23]])))

})

context("Dimension reduced")
test_that("dim reduce works", {
  expect_equal(dimension_reducer(test[1,],0.8), testing[[1]])
  expect_equal(dimension_reducer(test[2,],0.8), testing[[2]])

})

